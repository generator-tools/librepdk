def value_to_grid(config, val):
	g = config.get_grid()
	r = int(round(val/g)*int(g))
	return r

def to_nm(config, val):
	if val == 0:
		return 0
	ret = val.m_as(config.ureg.m)/config.get_dbunit()
	return value_to_grid(config, ret)

def snap_to_grid(config, val):
	x = value_to_grid(config, val[0])
	y = value_to_grid(config, val[1])
	return [x,y]


