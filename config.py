from glob import glob
import os
import sys

from importlib.machinery import SourceFileLoader

from lclayout.tech_util import load_tech_file

import pint

import librepdk.technologies

def available_technologies():
	techfile_filter = os.path.join("librepdk","technologies","*.tech.*")
	# find technology files
	techfiles=[]
	for path in sys.path:
		techfiles += glob(os.path.join(path,techfile_filter))
	# extract names
	technologies = []
	for i, file in enumerate(techfiles):
		module = SourceFileLoader(str(i), file).load_module()
		if hasattr(module, "name"):
			technologies.append(module.name)
	return technologies

class Config:
	confname = None
	tech_data = None
	electric_data = None

	def __init__(self, process):
		self.ureg = pint.UnitRegistry()
		self.ureg.autoconvert_offset_to_baseunit = True
		self.process = process
		techfile_filter=os.path.join("librepdk","technologies","*.tech.*")
		electric_filter=os.path.join("librepdk","technologies","*.electric.*")
		techfiles=[]
		electricfiles=[]
		for path in sys.path:
			techfiles += glob(os.path.join(path,techfile_filter))
			electricfiles += glob(os.path.join(path,electric_filter))

		# find technology files
		technologies = []
		for i, file in enumerate(techfiles):
			module = SourceFileLoader(str(i), file).load_module()
			if hasattr(module, "name"):
				technologies.append(module.name)
				if module.name == process:
					self.tech_data = load_tech_file(file)

		# find electric specs
		for i, file in enumerate(electricfiles):
			module = SourceFileLoader(str(i), file).load_module()
			if module.name == process:
				self.electric_data = module

		if process not in technologies:
			message = "Invalid technology: "+process+"!\nValid technologies are: \n"
			for i, t in enumerate(technologies):
				message += str(i+1)+". "+t+"\n"
			raise Exception(message)

		assert(self.tech_data is not None)

	def get_process_name(self):
		return self.tech_data.name if hasattr(self.tech_data, "name") else "UNDEF"

	def get_dbunit(self):
		return self.tech_data.db_unit

	def get_lambda(self):
		return self.tech_data.db_unit*self.tech_data.l*self.config.ureg.nm

	# --------------------------------------
	def get_min_enclosure(self, layer_pair):
		w = None
		if hasattr(self.tech_data, "minimum_enclosure"):
			if layer_pair in self.tech_data.minimum_enclosure:
				w = self.tech_data.minimum_enclosure[layer_pair]*self.ureg.nm
			else:
				print("WARNING: No minimum enclosue for",layer_pair,"defined!")
		else:
			print("WARNING: No minimum_enclosure defined!")
		return w

	def get_abutment_box(self):
		r = (0,0)
		if hasattr(self.tech_data, "my_abutment_box"):
			r = self.tech_data.my_abutment_box
		else:
			print("WARNING: No abutment box defined!")
		return r

	def get_outline(self):
		r = (0,0)
		if hasattr(self.tech_data, "my_outline"):
			r = self.tech_data.my_outline
		else:
			print("WARNING: No outline defined!")
		return r

	def get_min_spacing(self, layer_pair):
		w = None
		if hasattr(self.tech_data, "min_spacing"):
			if layer_pair in self.tech_data.min_spacing:
				w = self.tech_data.min_spacing[layer_pair]*self.ureg.nm
			else:
				print("WARNING: No minimum spacing for",layer_pair,"defined!")
		else:
			print("WARNING: No min_spacing defined!")

		return w

	def get_grid(self):
		g = 1
		if hasattr(self.tech_data, "grid"):
			g = self.tech_data.grid
		return int(g)

	def get_min_width(self, layer):
		w = 1 * self.ureg.nm
		if hasattr(self.tech_data, "minimum_width"):
			if layer in self.tech_data.minimum_width:
				w = self.tech_data.minimum_width[layer]*self.ureg.nm
			else:
				print("WARNING: No minimum width for",layer,"defined!")
		else:
			print("WARNING: No minimum_width defined!")

		return w

	def get_layer_id(self, layer):
		ret = None
		if hasattr(self.tech_data, "output_map"):
			if layer in self.tech_data.output_map:
				ret = self.tech_data.output_map[layer]
			else:
				print("WARNING: layer definition for",layer,"not found in output map!")
		else:
			print("WARNING: output map not defined!")

		return ret

	def get_via_size(self, name):
		ret = 1*self.ureg.nm
		if hasattr(self.tech_data, "via_size"):
			if name in self.tech_data.via_size:
				ret = float(self.tech_data.via_size[name])*self.ureg.nm
			else:
				print("WARNING: Via size for",name,"not defined!")
		else:
			print("WARNING: via_size not defined!")

		return ret

	def get_resistor_sheetres(self, name):
		x = 1
		y = 1

		if hasattr(self.tech_data, "weights_horizontal"):
			if name in self.tech_data.weights_horizontal:
				x = float(self.tech_data.weights_horizontal[name])*self.ureg.milliohm # they're using mOhm in LibreCell
			else:
				print("WARNING: Horizontal sheet resitance for",name,"not defined!")
		else:
			print("WARNING: weights_horizontal not defined!")

		if hasattr(self.tech_data, "weights_vertical"):
			if name in self.tech_data.weights_vertical:
				y = float(self.tech_data.weights_vertical[name])*self.ureg.milliohm # they're using mOhm in LibreCell
			else:
				print("WARNING: Vertical sheet resitance for",name,"not defined!")
		else:
			print("WARNING: weights_vertical not defined!")

		return (x,y)
