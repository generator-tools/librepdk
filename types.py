from enum import Enum, auto

class Direction(Enum):
	UP = auto()
	DOWN = auto()
	LEFT = auto()
	RIGHT = auto()
	VERTICAL = auto()
	HORIZONTAL = auto()

class ChannelType(Enum):
	PMOS = auto()
	NMOS = auto()
