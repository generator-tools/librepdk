import gdsfactory as gf

from librepdk.structure import LibrePDKStructure
from librepdk.contact import ViaStripe
from librepdk.types import ChannelType

import librepdk.helper as lph

class FET(LibrePDKStructure):

	def __init__(self, config, name, mtype, dims, contact_to=None, have_bulk_tap=False):
		LibrePDKStructure.__init__(self, config, name)
		self.have_bulk_tap = have_bulk_tap # enable tap for bulk

		self.N = dims[0]
		self.W = dims[1]
		self.L = dims[2]

		self.conlayer_name = config.get_pad_connection_layer() if contact_to is None else contact_to # Alternative contact layer

		# select well and implant
		if mtype == ChannelType.PMOS:
			self.well_layer_name = "nwell"
			self.implant_layer_name = 'pdiffusion'
			self.diff_contact_name = "pdiff_contact"
			self.plus_layer_name = 'pplus'
		elif mtype == ChannelType.NMOS:
			self.well_layer_name = "pwell"
			self.implant_layer_name = 'ndiffusion'
			self.diff_contact_name = "ndiff_contact"
			self.plus_layer_name = 'nplus'
		else:
			raise Exception("It has to be either NMOS or PMOS! This should NEVER happen!")

		self.implant_layer = config.get_layer_id(self.implant_layer_name)
		self.well_layer = config.get_layer_id(self.well_layer_name)
		self.pdiff_contact_layer = config.get_layer_id(self.diff_contact_name)
		self.plus_layer = config.get_layer_id(self.plus_layer_name)
		via_size = self.config.get_via_size(self.diff_contact_name)

		# those are LCLayout standard:
		self.gate_layer = config.get_layer_id("poly")
		self.poly_contact_layer = config.get_layer_id("poly_contact")

		# name "poly" is an lclayout standard
		w = config.get_min_width("poly")
		self.polyw = config.get_lambda() if w is None else w
		
		#self.generate_mos(mtype, dims)
		self.actual_transistor()

	'''
	 Build transistor structure and place the taps
	 pin 1: gate
	 pin 2: source
	 pin 3: drain
	 pin 4: bulk
	 w/l: width and length of the channel
	'''
	def actual_transistor(self):
		# spacing of the metal interconnect layer
		sp = self.config.get_min_spacing((self.conlayer_name, self.conlayer_name))
		spf = lph.to_nm(self.config, sp)
		w = self.polyw*self.W
		l = self.polyw*self.L

		# minimum enclosure
		me = self.config.get_min_enclosure((self.well_layer_name, self.implant_layer_name))
		if me is None:
			me = self.config.get_lambda()
		mef = lph.to_nm(self.config, me)

		# adding taps
		via_name = self.name+"_TAP"
		ws = self.config.get_min_width(self.conlayer_name)
		drain_source_tap = ViaStripe(self.config, via_name+"_ds", self.implant_layer_name, l, ws, self.conlayer_name, less_vias=True) # drain/source
		hp = w*self.N
		if self.N > 1:
			hp += self.N*ws
		gate_tap = ViaStripe(self.config, via_name+"_gate", "poly", ws, hp, self.conlayer_name, less_vias=True) # gate

		# determine implant width/height
		wi = lph.to_nm(self.config, l)
		hi = lph.to_nm(self.config, w if sp < w else sp)

		# determine gate width/height
		wp = drain_source_tap.xsize+lph.to_nm(self.config, sp)
		hp = lph.to_nm(self.config, w)

		# implant and taps
		implant = gf.components.rectangle(size=(wi,hi), layer=self.implant_layer)
		gate = gf.components.rectangle(size=(wp+2*gate_tap.xsize,hp), layer=self.gate_layer)

		# determine well width/height
		ww = gate_tap.xsize+wp+lph.to_nm(self.config, 2*me)
		hw = drain_source_tap.ysize+(drain_source_tap.ysize+hi)*self.N+lph.to_nm(self.config, 2*me)
		dy = mef+spf
		# place the bulk tap
		if self.have_bulk_tap:
			# add bulk contact
			bw = (wp+gate_tap.xsize)*self.ureg.nm
			bulk_tap = ViaStripe(self.config, via_name+"_bulk", self.well_layer_name, bw, ws, self.conlayer_name, less_vias=True) # bulk
			self.add_pin(4, self << bulk_tap.move([mef,mef]))
			hw += lph.to_nm(self.config, sp)+bulk_tap.ysize # adjust well height
			dy += bulk_tap.ysize

		# Add the well
		well = gf.components.rectangle(size=(ww,hw), layer=self.well_layer)
		self << well

		if self.plus_layer is not None:
			# Add the nplus/pplus
			plus_size=gate_tap.xsize # This is wrong and needs to be calculated properly
			plus = gf.components.rectangle(size=(ww+2*plus_size,hw+2*plus_size), layer=self.plus_layer)
			self << plus.move([-plus_size,-plus_size])

		if self.N > 1:
			tap = gf.components.pad(size=s, layer=lid)
			self.add_pin(2, self << tap)
			tap = gf.components.pad(size=s, layer=lid)
			self.add_pin(3, self << tap)
		else:
			self.add_pin(2, self << drain_source_tap.move([mef, dy]))

		dy += drain_source_tap.ysize
		self << implant.move([drain_source_tap.xsize-implant.xsize+mef, dy])

		gy = dy+(implant.ysize-hp)/2

		self.add_pin(1, self << gate_tap.move([mef+wp,gy]))
		for i in range(self.N):
			self << gate.move([mef-gate_tap.xsize,gy])
			gy+=hp+drain_source_tap.ysize

		if self.N == 1:
			dy += implant.ysize
			self.add_pin(3, self << drain_source_tap.move([mef, dy]))
