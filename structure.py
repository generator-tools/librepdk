import gdsfactory as gf
import pint

import librepdk.helper as lph

class LibrePDKStructure(gf.Component):
	def __init__(self, config, name):
		gf.Component.__init__(self, name)

		self.name = name
		self.ureg = config.ureg
		self.config = config

	def add_pin(self, number, pin):
		ports = {}
		for pn in pin.ports:
			self.add_port(port=pin.ports[pn], name=pn+'_'+str(number))

	def add_pin_to(self, o, number, pin):
		ports = {}
		for pn in pin.ports:
			o.add_port(port=pin.ports[pn], name=pn+'_'+str(number))

	def wire_up(self, o):
		self.add_ports(o.ports)
