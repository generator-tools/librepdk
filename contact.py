import gdsfactory as gf
from gdsfactory.components.compass import compass

from librepdk.structure import LibrePDKStructure
import librepdk.helper as lph

min_vias = 1

device_layers = ['nwell','ndiffusion','pdiffusion', 'poly', 'pwell',]

def get_biggest_via(config, from_layer, conlayer_name):
	if from_layer in device_layers:
		origlayer = 1
	else:
		origlayer = int(from_layer.replace("metal","").strip())

	# what's the highest metal layer number?
	toplayer = int(conlayer_name.replace("metal","").strip())

	maxdim = 0
	spacing = 0
	for lidx in range(origlayer, toplayer):
		vln = "via"+str(lidx)
		vdim = config.get_via_size(vln)
		space = config.get_min_spacing((vln,vln))
		# find spacing of largest via layer
		if vdim > maxdim:
			maxdim = vdim
			spacing = space

	return maxdim, spacing

def get_min_viagroup_pad_size(config, from_layer, conlayer_name):
	maxdim, spacing = get_biggest_via(config, from_layer, conlayer_name)
	return min_vias*(maxdim+spacing)

class ViaStripe(LibrePDKStructure):
	def __init__(self, config, name, from_layer, w, h, conlayer_name, less_vias=False):
		LibrePDKStructure.__init__(self, config, name)
		# the layers:
		self.from_layer = from_layer
		self.conlayer_name = conlayer_name
		self.conlayer = self.config.get_layer_id(self.conlayer_name)
		self.less_vias = less_vias
		self.get_via_stripe(w, h)

	def get_via_stripe(self, w, h):
		viastackup = []
		metalstackup = []

		if self.from_layer in device_layers:
			origlayer = 1
			metalstackup.append(self.from_layer)
		else:
			origlayer = int(self.from_layer.replace("metal","").strip())

		# what's the highest metal layer number?
		toplayer = int(self.conlayer_name.replace("metal","").strip())

		match self.from_layer:
			case 'ndiffusion':
				viastackup.append("ndiff_contact")
			case 'pdiffusion':
				viastackup.append("pdiff_contact")
			case 'poly':
				viastackup.append("poly_contact")

		for lidx in range(origlayer, toplayer):
			viastackup.append("via"+str(lidx))
		for lidx in range(origlayer, toplayer):
			metalstackup.append("metal"+str(lidx))

		# the interconnects
		via = gf.Component(self.name+"_single_via")

		maxdim, spacing = get_biggest_via(self.config, self.from_layer, self.conlayer_name)

		for vln in viastackup:
			vlid = self.config.get_layer_id(vln)
			space = self.config.get_min_spacing((vln,vln))
			vdim = self.config.get_via_size(vln)
			vs = lph.to_nm(self.config, vdim)
			vr = gf.components.rectangle(size=(vs,vs), layer=vlid)
			dxy = lph.to_nm(self.config, -vdim/2)
			via << vr.move([dxy,dxy])
			# find spacing of largest via layer
			if vdim > maxdim:
				maxdim = vdim
				spacing = space

		if maxdim > 0:
			col = w/(spacing+maxdim)
			col = int(col)

			if col < min_vias:
				col = min_vias

			row = h/(spacing+maxdim)
			row = int(row)

			if row < min_vias:
				row = min_vias

			if (maxdim+spacing)*col < w and not self.less_vias:
				col += 1

			if(maxdim+spacing)*row < h and not self.less_vias:
				row += 1

			# the metal rectancles on the various layers
			wn = (maxdim+spacing)*col
			hn = (maxdim+spacing)*row
			if self.less_vias:
				w = wn if w < wn else w
				h = hn if h < hn else h
			else:
				w = wn
				h = hn

			# centering the via group
			sp = lph.to_nm(self.config, spacing+maxdim)
			varr = gf.components.array(component=via, spacing=(sp, sp), columns=col, rows=row)
			if self.less_vias:
				dcpx = (lph.to_nm(self.config, w+spacing)-varr.xsize)/2
				dcpy = (lph.to_nm(self.config, h+spacing)-varr.ysize)/2
			else:
				dcpx = dcpy = lph.to_nm(self.config, (maxdim+spacing)/2)

			self << varr.move(lph.snap_to_grid(self.config, [dcpx, dcpy]))

		s = (lph.to_nm(self.config, w),lph.to_nm(self.config, h))
		for mln in metalstackup:
			lid = self.config.get_layer_id(mln)
			r1 = gf.components.rectangle(size=s, layer=lid)
			self << r1

		lid = self.config.get_layer_id("metal"+str(toplayer))

		pad = gf.components.pad(size=s, layer=lid)
		pad = pad.move([-pad.xmin,-pad.ymin])
		c_ref = self.add_ref(pad)
		self.add_ports(c_ref.ports)
