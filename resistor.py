import numpy as np
import math

import phidl.geometry as pg
import phidl.path as pp
from phidl import Device

import gdsfactory as gf

from librepdk.structure import LibrePDKStructure
from librepdk.types import Direction
import gdsfactory as gf

import librepdk.helper as lph

from librepdk.contact import ViaStripe, get_min_viagroup_pad_size

class StripResistor(LibrePDKStructure):

	def __init__(self, config, name, layer, width, R, direction=Direction.HORIZONTAL, contact_to=None):
		LibrePDKStructure.__init__(self, config, name)
		assert(direction == Direction.VERTICAL or direction == Direction.HORIZONTAL)

		self.target_r = R
		self.layer = layer
		self.direction = direction
		self.width = width
		self.contact_to = contact_to

		sheetrs = self.config.get_resistor_sheetres(self.layer)
		if direction == Direction.HORIZONTAL:
			self.R_sq = sheetrs[0]
		elif direction == Direction.VERTICAL:
			self.R_sq = sheetrs[1]

		self.wire_up(self << self.get_structure())

	def add_contacts(self, r):
		ret = gf.Component(self.name, with_uuid=True)
		ps = self.config.get_min_width(self.layer) if self.layer == self.contact_to else get_min_viagroup_pad_size(self.config, self.layer, self.contact_to)
		pad = ViaStripe(self.config, self.name+"_CONTACT", self.layer, ps, self.width, self.contact_to, less_vias=True)
		mx = pad.xsize
		my = lph.value_to_grid(self.config, (pad.ysize-r.ysize)/2)
		py = pad.ysize
		x2 = pad.xsize+r.xsize
		ret << r.move([mx,my])
		self.add_pin_to(ret, 1, ret << pad)
		self.add_pin_to(ret, 2, ret << pad.move([x2, 0]))
		return ret

	def get_structure(self):
		w = self.width*(self.target_r/self.R_sq)
		l = lph.to_nm(self.config, self.width)
		w = lph.to_nm(self.config, w)
		lid = self.config.get_layer_id(self.layer)
		res = gf.components.rectangle(size=(w, l), layer=lid)

		if self.contact_to is not None:
			res = self.add_contacts(res)
		# Correct orientation:
		if self.direction == Direction.VERTICAL:
			res = res.rotate(90)

		return res.move([-res.xmin,-res.ymin])

	def get_wire_width(self):
		return self.width

class MeanderResistor(LibrePDKStructure):

	def __init__(self, config, name, layer, width, R, direction=Direction.HORIZONTAL, contact_to=None, flip=False):
		LibrePDKStructure.__init__(self, config, name)
		assert(direction == Direction.VERTICAL or direction == Direction.HORIZONTAL)

		self.target_r = R
		self.layer = layer
		self.direction = direction
		self.width = width
		self.contact_to = contact_to

		sheetrs = self.config.get_resistor_sheetres(self.layer)
		if direction == Direction.HORIZONTAL:
			self.R_sH = sheetrs[0]
			self.R_sV = sheetrs[1]
		elif direction == Direction.VERTICAL:
			self.R_sH = sheetrs[1]
			self.R_sV = sheetrs[0]
		else:
			raise Exception("Not a valid direction!")

		res = gf.Component(name, with_uuid=True)
		r = self.get_structure() # create meander
		y = x = 0
		# add pins if needed
		if self.contact_to is not None:
			wf = lph.to_nm(self.config, width)
			ps = self.config.get_min_width(self.layer) if self.layer == self.contact_to else get_min_viagroup_pad_size(self.config, self.layer, self.contact_to)
			pad = ViaStripe(self.config, self.name+"_CONTACT", self.layer, ps, self.width, self.contact_to, less_vias=True)
			y = pad.ysize-wf if wf < pad.ysize else 0
			self.add_pin_to(res, 1, res << pad.move([0, 0]))
			self.add_pin_to(res, 2, res << pad.move([0, y+r.ysize-wf]))
			x = pad.xsize

		res << r.move([x,y])

		# Correct orientation:
		if direction == Direction.VERTICAL:
			res = res.rotate(90)
		if flip:
			res = res.rotate(180)

		self.wire_up(self << res.move([-res.xmin,-res.ymin])) # add structure

	def calculate_meander_points(self):
		n = self.dermine_n() # calculate n
		# calculating the difference in resistance due to squary quantization
		# adding it as additional offset top and bottom
		dR = self.target_r-self.calculate_r()
		dx = (self.width*(dR/self.R_sH))/2
		points=[]
		xoff = self.width if n%2==0 else -self.width/2
		xoff += dx
		x1 = xoff
		x1 = lph.to_nm(self.config, x1)
		x2 = xoff
		x2 += n*self.width
		x2 = lph.to_nm(self.config, x2)
		dh = self.width*2
		l = int((n+1)/2 if n%2==1 else n/2)
		for i in range(0, l):
			y = lph.to_nm(self.config, i*dh)
			if i==0 or i+1==l:
				p1 = (0, y)
				p2 = (x2, y)
			else:
				p1 = (x1, y)
				p2 = (x2, y)

			if i%2 == 0:
				points.append(p1)
				points.append(p2)
			else:
				points.append(p2)
				points.append(p1)

		return np.array(points)

	def calculate_r(self):
		n = self.dermine_n()
		R_sH = self.R_sH
		R_sV = self.R_sV
		R_H = ((n+1)/2)*(n-1)*R_sH-R_sH
		R_V = (n-1)*R_sV
		return R_H+R_V

	def dermine_n(self):
		R_sH = self.R_sH
		R_sV = self.R_sV
		R = self.target_r
		return int(((R_sV**2+2*R_sH*R_sV+3*R_sH**2+2*R*R_sH)**.5-R_sV)/R_sH)

	def get_wire_width(self):
		return self.width

	def get_structure(self):
		r = gf.Component(self.name, with_uuid=True)
		lid = self.config.get_layer_id(self.layer)
		points = self.calculate_meander_points()
		P = pp.Path(points)
		P = P.extrude(width=lph.to_nm(self.config,self.width), layer=lid)
		s = gf.read.from_phidl(P)
		r.add(s)
		return r.move([-r.xmin,-r.ymin])
